from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Persona)
admin.site.register(models.Usuario)
admin.site.register(models.Servicio)
admin.site.register(models.Cola)
admin.site.register(models.Ticket)