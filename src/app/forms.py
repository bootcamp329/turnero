from django import forms
from django.views.generic.edit import UpdateView
from .models import *

class PersonaModelForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = ["cedula","nombre","apellido","telefono","email"]


class TicketModelForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ["id","cedula_persona","prioridad","id_cola"]


class UsuarioModelForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ["rol","activo","cedula_persona"]


class ServicioModelForm(forms.ModelForm):
    class Meta:
        model = Servicio
        fields = ["nombre"]


class ColaModelForm(forms.ModelForm):
    class Meta:
        model = Cola
        fields = ["numero","id_usuario","id_servicio"]
