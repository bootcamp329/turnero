from queue import PriorityQueue

class TicketsPrioridad(object):
    def __init__(self, prioridad,id, cedula_persona, id_cola):
        self.prioridad = prioridad
        self.id = id
        self.cedula_persona = cedula_persona
        self.id_cola = id_cola

    def __lt__(self, otro):
        return self.prioridad < otro.prioridad
