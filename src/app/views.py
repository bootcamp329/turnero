from django.shortcuts import render,redirect
from .forms import *
from .models import *
from .colaPrioridad import *
from queue import PriorityQueue

# Create your views here.
def inicio(request):
    titulo = "Sistema de Turnero"
    saludo = "Favor, loguearse"
    if request.user.is_authenticated:
        saludo = "Bienvenido %s" %(request.user)
    context = {
        "titulo" : titulo,
        "subtitulo" : saludo,
    }

    return render(request, "inicio.html", context)


# ABM Persona
def persona(request):
    personasListadas = Persona.objects.all()
    form = PersonaModelForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        form = PersonaModelForm()
        print(instance)
    titulo = "Formulario Persona"
    saludo = "Favor, loguearse"
    if request.user.is_authenticated:
        saludo = "Bienvenido %s" %(request.user)
    context = {
        "titulo" : titulo,
        "subtitulo" : saludo,
        "form" : form,
        "personas": personasListadas,
    }

    return render(request, "persona.html", context)


def editar_persona(request, pk):
    persona = Persona.objects.get(cedula=pk)
    form = PersonaModelForm(instance=persona)
    if request.method == 'POST':
        form = PersonaModelForm(request.POST, instance=persona)
        if form.is_valid():
            form.save()
            return redirect('Persona')
    titulo = "Editar Persona"
    context = {
        "titulo" : titulo,
        'persona': persona,
        'form': form,
    }

    return render(request, 'editar-persona.html', context)


def eliminar_persona(request, pk):
    persona = Persona.objects.get(cedula=pk)
    if request.method == 'POST':
      persona.delete()
      return redirect('Persona')
    context = {
        'persona': persona,
    }
    return render(request, 'eliminar-persona.html', context)


# ABM Usuario
def usuario(request):
    usuariosListados = Usuario.objects.all()
    form = UsuarioModelForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        form = UsuarioModelForm()
        print(instance)
    titulo = "Formulario Usuario"
    saludo = "Favor, loguearse"
    if request.user.is_authenticated:
        saludo = "Bienvenido %s" %(request.user)
    context = {
        "titulo" : titulo,
        "subtitulo" : saludo,
        "form" : form,
        "usuarios": usuariosListados,
    }

    return render(request, "usuario.html", context)


def editar_usuario(request, pk):
    usuario = Usuario.objects.get(id=pk)
    form = UsuarioModelForm(instance=usuario)
    if request.method == 'POST':
        form = UsuarioModelForm(request.POST, instance=usuario)
        if form.is_valid():
            form.save()
            return redirect('Usuario')
    titulo = "Editar Usuario"
    context = {
        "titulo" : titulo,
        'usuario': usuario,
        'form': form,
    }

    return render(request, 'editar-usuario.html', context)


def eliminar_usuario(request, pk):
    usuario = Usuario.objects.get(id=pk)
    if request.method == 'POST':
      usuario.delete()
      return redirect('Usuario')
    context = {
        'usuario': usuario,
    }
    return render(request, 'eliminar-usuario.html', context)


# ABM Servicio
def servicio(request):
    serviciosListados = Servicio.objects.all()
    form = ServicioModelForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        form = ServicioModelForm()
        print(instance)
    titulo = "Formulario Servicio"
    saludo = "Favor, loguearse"
    if request.user.is_authenticated:
        saludo = "Bienvenido %s" %(request.user)
    context = {
        "titulo" : titulo,
        "subtitulo" : saludo,
        "form" : form,
        "servicios": serviciosListados,
    }

    return render(request, "servicio.html", context)


def editar_servicio(request, pk):
    servicio = Servicio.objects.get(id=pk)
    form = ServicioModelForm(instance=servicio)
    if request.method == 'POST':
        form = ServicioModelForm(request.POST, instance=servicio)
        if form.is_valid():
            form.save()
            return redirect('Servicio')
    titulo = "Editar Servicio"
    context = {
        "titulo" : titulo,
        'servicio': servicio,
        'form': form,
    }

    return render(request, 'editar-servicio.html', context)


def eliminar_servicio(request, pk):
    servicio = Servicio.objects.get(id=pk)
    if request.method == 'POST':
      servicio.delete()
      return redirect('Servicio')
    context = {
        'servicio': servicio,
    }
    return render(request, 'eliminar-servicio.html', context)


# ABM Cola
def cola(request):
    colasListadas = Cola.objects.all()
    form = ColaModelForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        form = ColaModelForm()
        print(instance)
    titulo = "Formulario Cola"
    saludo = "Favor, loguearse"
    if request.user.is_authenticated:
        saludo = "Bienvenido %s" %(request.user)
    context = {
        "titulo" : titulo,
        "subtitulo" : saludo,
        "form" : form,
        "colas": colasListadas,
    }

    return render(request, "cola.html", context)


def editar_cola(request, pk):
    cola = Cola.objects.get(id=pk)
    form = ColaModelForm(instance=cola)
    if request.method == 'POST':
        form = ColaModelForm(request.POST, instance=cola)
        if form.is_valid():
            form.save()
            return redirect('Cola')
    titulo = "Editar Cola"
    context = {
        "titulo" : titulo,
        'cola': cola,
        'form': form,
    }

    return render(request, 'editar-cola.html', context)


def eliminar_cola(request, pk):
    cola = Cola.objects.get(id=pk)
    if request.method == 'POST':
      cola.delete()
      return redirect('Cola')
    context = {
        'cola': cola,
    }
    return render(request, 'eliminar-cola.html', context)


# ABM Ticket
def ticket(request):
    form = TicketModelForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        form = TicketModelForm()
        print(instance)
    titulo = "Formulario Ticket"
    saludo = "Favor, loguearse"
    if request.user.is_authenticated:
        saludo = "Bienvenido %s" %(request.user)
    context = {
        "titulo" : titulo,
        "subtitulo" : saludo,
        "form" : form,
    }

    return render(request, "ticket.html", context)


def derivar_ticket(request, pk):
    ticket = Ticket.objects.get(id=pk)
    form = TicketModelForm(instance=ticket)
    if request.method == 'POST':
        form = TicketModelForm(request.POST, instance=ticket)
        if form.is_valid():
            form.save()
            return redirect('GestionCola')
    titulo = "Derivar Ticket"
    context = {
        "titulo" : titulo,
        'ticket': ticket,
        'form': form,
    }

    return render(request, 'derivar-ticket.html', context)


def atender_ticket(request, pk):
    ticket = Ticket.objects.get(id=pk)
    if request.method == 'POST':
      ticket.delete()
      return redirect('GestionCola')
    context = {
        'ticket': ticket,
    }
    return render(request, 'atender-ticket.html', context)


# Gestion de cola
def gestionCola(request):
    colasListadas = Cola.objects.all()
    ticketsListados = Ticket.objects.all()
    tickets = PriorityQueue()
    colaOrdenada = []
    for ticket in ticketsListados:
        tickets.put(TicketsPrioridad(ticket.prioridad, ticket.id, ticket.cedula_persona, ticket.id_cola))

    while not tickets.empty():
        t = tickets.get()
        print(t.prioridad, t.cedula_persona)
        colaOrdenada.append(t)

    if request.method == 'POST':
      id_cola = request.POST['select-cola']
      print(id_cola)
      if id_cola != 'Seleccionar cola':
        ticketsListados = Ticket.objects.filter(id_cola=id_cola)
        colaOrdenada = []
        for ticket in ticketsListados:
            tickets.put(TicketsPrioridad(ticket.prioridad, ticket.id, ticket.cedula_persona, ticket.id_cola))

        while not tickets.empty():
            t = tickets.get()
            print(t.prioridad, t.cedula_persona)
            colaOrdenada.append(t)

      else:
        ticketsListados = Ticket.objects.all()
        colaOrdenada = []
        for ticket in ticketsListados:
            tickets.put(TicketsPrioridad(ticket.prioridad, ticket.id, ticket.cedula_persona, ticket.id_cola))

        while not tickets.empty():
            t = tickets.get()
            print(t.prioridad, t.cedula_persona)
            colaOrdenada.append(t)
    context = {
      "colas": colasListadas,
      "tickets": colaOrdenada,
    }
    return render(request, "gestionCola.html", context)