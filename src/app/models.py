from django.db import models

# Create your models here.

class Persona(models.Model):
    cedula = models.CharField(max_length=16,primary_key=True)
    nombre = models.CharField(max_length=30)
    apellido = models.CharField(max_length=30)
    telefono = models.CharField(max_length=16, null=True, blank=True)
    email = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self) -> str:
        return self.cedula


class Usuario(models.Model):
    rol = models.CharField(max_length=40)
    activo = models.CharField(max_length=2)
    cedula_persona = models.ForeignKey(Persona, on_delete=models.CASCADE, db_column='cedula_persona')

    def __str__(self) -> str:
        texto = "{0} ({1})"
        return texto.format(self.rol, self.cedula_persona)


class Servicio(models.Model):
    nombre = models.CharField(max_length=60)

    def __str__(self) -> str:
        return self.nombre


class Cola(models.Model):
    numero = models.IntegerField()
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, db_column='id_usuario')
    id_servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE, db_column='id_servicio')

    def __str__(self) -> str:
        return str(self.numero)


class Ticket(models.Model):
    prioridad = models.CharField(max_length=20)
    cedula_persona = models.ForeignKey(Persona, on_delete=models.CASCADE, db_column='cedula_persona')
    id_cola = models.ForeignKey(Cola, on_delete=models.CASCADE, db_column='id_cola')

    def __str__(self) -> str:
        return str(self.cedula_persona)
