"""turnero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.inicio, name='Inicio'),
    path('persona/',views.persona, name='Persona'),
    path('usuario/',views.usuario, name='Usuario'),
    path('servicio/',views.servicio, name='Servicio'),
    path('cola/',views.cola, name='Cola'),
    path('ticket/',views.ticket, name='Ticket'),
    path('gestionCola/',views.gestionCola, name='GestionCola'),
    path('editar-persona/<str:pk>',views.editar_persona, name='EditarPersona'),
    path('eliminar-persona/<str:pk>',views.eliminar_persona, name='EliminarPersona'),
    path('editar-usuario/<str:pk>',views.editar_usuario, name='EditarUsuario'),
    path('eliminar-usuario/<str:pk>',views.eliminar_usuario, name='EliminarUsuario'),
    path('editar-servicio/<str:pk>',views.editar_servicio, name='EditarServicio'),
    path('eliminar-servicio/<str:pk>',views.eliminar_servicio, name='EliminarServicio'),
    path('editar-cola/<str:pk>',views.editar_cola, name='EditarCola'),
    path('eliminar-cola/<str:pk>',views.eliminar_cola, name='EliminarCola'),
    path('derivar-ticket/<str:pk>',views.derivar_ticket, name='DerivarTicket'),
    path('atender-ticket/<str:pk>',views.atender_ticket, name='AtenderTicket'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)