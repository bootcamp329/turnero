<h1 align="center"> Proyecto Turnero </h1>
<p> Proyecto de turnero web desarrollado con Django y utilizando el sistema de base de datos PostgreSQL </p>
<h4 align="center">
:construction: Proyecto bajo revisión :construction:
</h4>
<h2>  :hammer: Fucionalidades del proyect </h2>
<ul>
<li> Sacar turno: Generación de ticket de atención. </li>
<li> Gestión de Personas: ABM tanto de personas, ya sean clientes o usuarios. </li>
<li> Gestión de Cola: ABM de colas de espera. </li>
<li> Gestión de Servicios: ABM de tipos de servicios ofrecidos. </li>
</ul>
<h2>  :pencil: Instrucciones de uso </h2>
<ul>
<li> Activar el entorno virtual con el comando respectivo <i>activate</i>. </li>
<li> Instalar las dependencias indicadas en <i>requirements.txt</i>. </li>
<li> Ejecutar el servidor a través de <i>python manage.py runserver</i>. </li>
<li> Iniciar sesión en <i>127.0.0.1:8000/admin</i> con usuario y contraseña <i>admin</i>. </li>
</ul>
<h2>  :heavy_check_mark: Tecnologías utilizadas <h2>
<ul>
<li> Django </li>
<li> PostgreSQL </li>
<li> Bootstrap </li>
</ul>
<h2> :boy: Autor </h2>
<sub>José Cabrera</sub> <br>(https://https://gitlab.com/josgacat)
